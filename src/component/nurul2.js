import React from 'react';
import {ScrollView, StyleSheet, Image, TouchableOpacity} from 'react-native';

const baten = (props) => {
    return(
        <ScrollView horizontal>
            <TouchableOpacity onPress={props.onButtonPress}>
                <Image source = {require('../assets/menyukai.jpg')} style ={styles.menyukai}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source = {require('../assets/komen.jpg')} style= {{width :30, height:35, marginTop : 6 }}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source= {require('../assets/bagikan.jpg')} style= {{width: 50, height:50, marginLeft: -3, marginTop :-3}}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Image source = {require('../assets/menyimpan.jpg')} style = {{width: 30, height:35, marginTop : 5, marginLeft:185}}/>
            </TouchableOpacity>
               
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    menyukai :{
        width : 50,
        height : 40,
        marginLeft : 5,
        marginTop : 1
    }
});
export default baten;