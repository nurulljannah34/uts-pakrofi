import React, { useState } from 'react' ;
import {View,Text, StyleSheet, ScrollView,Image} from 'react-native';
import Nurul2 from './nurul2';
import Nurul3 from './nurul3';



const Flexbox = () => {
    const [nurul,setNurul] =useState(0);
    return(
        <View style={styles.container}> 
        <View style={styles.flexsatu}>
        <Image source={require('../assets/insta.jpg')} style={{width:150,height:60}}/>
        <Image source={require('../assets/love.jpg')} style={{width:50,height:60 ,marginLeft:110}}/>
        <Image source={require('../assets/massage.jpg')} style={{width:40,height:60}}/>
       
        </View>
        <View style={styles.flexdua}>
            <ScrollView>
                <ScrollView horizontal>
                <Image source={require('../assets/poto.jpg')} style={styles.poto}/>
                <Image source={require('../assets/poto.jpg')} style={styles.poto}/>
                <Image source={require('../assets/poto.jpg')} style={styles.poto}/>
                <Image source={require('../assets/poto.jpg')} style={styles.poto}/>
                <Image source={require('../assets/poto.jpg')} style={styles.poto}/>
                <Image source={require('../assets/poto.jpg')} style={styles.poto}/>
                </ScrollView>
                <Image source={require('../assets/isi.jpg')} style={styles.isi}/>
                <Nurul2 onButtonPress= {() => setNurul(nurul+1)}/>
                <Nurul3 tulis={nurul}/>
                <Image source={require('../assets/men.jpg')} style={{width:350,height :100,marginLeft: 10}}/>
                <Image source={require('../assets/isi.jpg')} style={styles.isi}/>
                <Nurul2 onButtonPress= {() => setNurul(nurul+1)}/>
                <Nurul3 tulis={nurul}/>
                <Image source={require('../assets/men.jpg')} style={{width:350,height :100,marginLeft: 10}}/>
                <Image source={require('../assets/isi.jpg')} style={styles.isi}/>
                <Nurul2 onButtonPress= {() => setNurul(nurul+1)}/>
                <Nurul3 tulis={nurul}/>
                <Image source={require('../assets/men.jpg')} style={{width:350,height :100,marginLeft: 10}}/>
                <Image source={require('../assets/isi.jpg')} style={styles.isi}/>
                <Nurul2 onButtonPress= {() => setNurul(nurul+1)}/>
                <Nurul3 tulis={nurul}/>
                <Image source={require('../assets/men.jpg')} style={{width:350,height :100,marginLeft: 10}}/>
                <Image source={require('../assets/isi.jpg')} style={styles.isi}/>
                <Nurul2 onButtonPress= {() => setNurul(nurul+1)}/>
                <Nurul3 tulis={nurul}/>
                <Image source={require('../assets/men.jpg')} style={{width:350,height :100,marginLeft: 10}}/>
            </ScrollView>
        </View>
        <View style={styles.flextiga}>
        <Image source={require('../assets/home.jpg')} style={styles.baw}/>
        <Image source={require('../assets/find.jpg')} style={styles.baw}/>
        <Image source={require('../assets/plus.jpg')} style={styles.baw}/>
        <Image source={require('../assets/belanja.jpg')} style={styles.baw}/>
        <Image source={require('../assets/profil.jpg')} style={styles.baw}/>
        </View>
    </View>


    );
};


const styles=StyleSheet.create({
    container:{
        flex:1
    },
    flexsatu:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'row'
    },
    flexdua:{
        flex:9,
        backgroundColor:'white',
    },
    flextiga:{
        flex:1,
        backgroundColor:'white',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems:'center'
    },
    isi:{
        width:359,
        height:520
    },
    poto:{
        width:60,
        height:60,
        borderRadius:30,
        marginLeft:8,
        borderWidth:2,
        borderColor:'red',
    },
    baw:{
        width:40,
        height:40
    }
});

export default Flexbox;